# Шаблон для практики по Ansible

## Что внутри?
- Пара докер-контейнеров для эмуляции машин
- Минимальный (почти) набор для автоматизации:
  - inventory -- [./ansible/inventory.yaml](ansible/inventory.yaml) -- описывает докер хосты;
  - playbook -- [./ansible/playbook.yaml](ansible/playbook.yaml) -- пара простых тасков для примера;
  - default cfg -- [./ansible.cfg](ansible.cfg) -- чтобы не указывать инвентори каждый раз.

## Как пользоваться?
1. Установить зависимости:
    - docker + docker-compose
    - ansible
    - sshpass (поскольку логин по паролю)
2. Запустить контейнеры
    ```
    $ docker-compose up --build -d
    ```
3. Проверить что все работает:
    ```
    $ ansible -m ping all
    ```
4. Запустить плейбук
    ```
    $ ansible-playbook ./ansible/playbook.yaml
    ```

## Что дальше?
Дописать таски:
- Копирование SSH ключей + конфиг `sshd`
- Установка удобных пакетов (`curl`, `vim`, `git`, …)
- Установка удобных алиасов и собственных утилит
- Выгрузка `/home/` с ВМки на хост / на трансфер / в телеграм
- Настройка сервиса для сбора трафика

## Полезные ссылки
- [Документация](https://docs.ansible.com/ansible/2.3/playbooks_intro.html)
- [Про when](https://docs.ansible.com/ansible/2.3/playbooks_conditionals.html "про when")
- [Добавление пользователей](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/user_module.html)

## Итак знаем что не работает
- **Q**: Докер под macOS (и, возможно виндовс?) не умеет вывешивать статические адреса, 
  потому подключиться по SSH не получается.
  
  **A**: В линуксе все работает!

